a) El análisis y Diseño del Problema utilizando el enfoque estructurado
a. Análisis de Requisitos
b. Diseño Estructurado:
i. Diagrama Entidad-Relación de información
ii. Diagrama de Flujo de Datos
c. Diseño Funcional
i. Diagramas de transición de estados
ii. Diagramas de flujo o algoritmos

Problema:

Se tiene un sistema de computadora que permite al usuario dibujar dentro de
una ventana, cualquier polígono de 3 o más lados pero también primas. Todo
polígono se formará de un conjunto de puntos y el usuario tendrá la opción de
dibujarlos, trasladarlos en el plano, así como calcular su área o perímetro. Un
punto se forma de una abscisa, una ordenada, se puede también dibujar,
trasladar y calcular su distancia respecto de otro punto. A su vez los prismas se
componen de cualquier polígono como base y un punto que representa su altura.
Cualquier polígono se puede trasladar en el plano y se puede calcular su
volumen. Concretamente, el usuario podrá dibujar polígonos cuadrados,
triángulos y pentágonos y primas triangulares, cuadrangulares y pentagonales.

a) Análisis de requisitos
Actores

Descripción
Persona que utilizará el sistema y podrá dibujar
dentro de una ventana, cualquier polígono y
prismas

Casos de Uso
C Crear punto

Descripción
Crea un punto, con base a las coordenadas
proporcionadas

CCrear polígono

Crea un polígono si hay tres a cinco puntos
dados

C Crea prisma

Si hay un polígono y altura disponibles, crea un
prisma
Calcula distancia entre dos puntos dados

C distancia entre
Calcula
dos puntos
C
Proporciona
abscisa

C
Proporciona
ordenada

C Calcula área
C
Calcula
perímetro

C
Proporciona
altura

Pide al usuario el valor de la abscisa para crear
el punto
Pide al usuario el valor de la ordenada para
crear el punto
Calcula el área de un polígono previamente
creado
Calcula perímetro de un polígono ya creado
Solicita al usuario el valor de la altura para crear
un prisma, debe existir un polígono previamente
Calcula el volumen del prima

CCalcula volumen

C

Dibujar

Dibuja las figuras en el lienzo
Permite al usuario mover las figuras por el lienzo

C Trasladar

Diagrama de casos de uso

b. Diseño Estructurado:
i. Diagrama Entidad-Relación de información

ii. Diagrama de Flujo de Datos

c. Diseño Funcional
i. Diagramas de transición de estados

ii. Diagramas de flujo o algoritmos

