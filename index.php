<?php
  class Diccionario{
    public $valores;
    public $texto;
    public $cadenaAuxiliar;
    public $contadorDeCaracteres;
    public $varAux;
    function __construct($texto){
      $this->texto = $texto;
    }
    function asociaArray(){
      $this->valores = array('Nivel Educativo:'=>"",
        'Nombre del Plan de Estudios:'=>"",
        'Modalidad Académica'=>"",
        'Asignatura:'=>"",
        'Ubicación:'=>"",
        'General:'=>"",
        'Específicos:'=>"");
      foreach ($this->valores as $key => $value) {
        //echo "LLAVE".utf8_decode($key)."<br><br><hr>";
        /*if(strpos($this->texto,utf8_decode($key))){
          echo "TRUE".utf8_decode($key)."<hr>";
        }*/
        //echo "TEXTO ".strlen($this->texto)."<br><br>";
        $cadenaAuxiliar = substr($this->texto, strpos($this->texto,utf8_decode($key))+strlen(utf8_decode($key)));
        $this->valores[$key] = $cadenaAuxiliar;
        $this->texto = $cadenaAuxiliar;
        //echo $this->texto;
        //echo "<br><br><br>";
      }
      $contadorDeCaracteres = 0;
      $this->valores['Específicos:']=substr($this->valores['Específicos:'], 0,strlen($this->valores['Específicos:'])-$contadorDeCaracteres);
      $contadorDeCaracteres += strlen($this->valores['Específicos:'])+strlen('Específicos:');
      //echo $this->valores['Específicos:'];
      $this->valores['General:']=substr($this->valores['General:'], 0,strlen($this->valores['General:'])-$contadorDeCaracteres);
      $contadorDeCaracteres += strlen($this->valores['General:']);
      //echo "<br><br>".$this->valores['General:'];

      $this->valores['Ubicación:']=substr($this->valores['Ubicación:'], 0,strlen($this->valores['Ubicación:'])-($contadorDeCaracteres+strlen(" OBJETIVOS: General: ")));
      $contadorDeCaracteres += strlen($this->valores['Ubicación:'])+strlen(" OBJETIVOS: General: ");
      //echo "<br><br>".$this->valores['Ubicación:'];

      $this->valores['Asignatura:']=substr($this->valores['Asignatura:'], 0,strlen($this->valores['Asignatura:'])-($contadorDeCaracteres+strlen("Ubicación:")));
      $contadorDeCaracteres += strlen($this->valores['Asignatura:'])+strlen("Ubicación:");
     // echo "<br><br>".$this->valores['Asignatura:'];

      $this->valores['Modalidad Académica']=substr($this->valores['Modalidad Académica'], 2,strlen($this->valores['Modalidad Académica'])-($contadorDeCaracteres+strlen(" Nombre de la Asignatura:  ")));
      $contadorDeCaracteres += strlen($this->valores['Modalidad Académica'])+strlen("Nombre de la Asignatura: ");
      //echo "<br><br>".$this->valores['Modalidad Académica'];

      $this->valores['Nombre del Plan de Estudios:']=substr($this->valores['Nombre del Plan de Estudios:'], 0,strlen($this->valores['Nombre del Plan de Estudios:'])-($contadorDeCaracteres+strlen("Modalidad Academica : ")));
      $contadorDeCaracteres += strlen($this->valores['Nombre del Plan de Estudios:'])+strlen("Modalidad Academica : ");
      //echo "<br><br>".$this->valores['Nombre del Plan de Estudios:'];

      $this->valores['Nivel Educativo:']=substr($this->valores['Nivel Educativo:'], 0,strlen($this->valores['Nivel Educativo:'])-($contadorDeCaracteres+strlen("Nombre del Plan de Estudios: ")));
      $contadorDeCaracteres += strlen($this->valores['Nivel Educativo:']);
      //echo "<br><br>".$this->valores['Nivel Educativo:'];
    }
  }
  require('class.pdf2text.php');//Agrega el archivo .class.pdf2text para poder usar la clase que esta dentro de ese archivo
  header('Content-Type: text/html; charset=ISO-8859-1');//Estandar para la codificacion de texto al mostrarlo
  $contenido = new PDF2Text();//Se instancia un objeto del tipo PDF2Text
  $contenido->setFilename('mineria.pdf');//La ruta del archivo .PDF que leera
  $contenido->decodePDF();//El texto enriquecido en PDF se decodifica en su simil texto plano
  $diccionario = new Diccionario($contenido->output());
  $diccionario->asociaArray();
  $template = file_get_contents('plantilla.html');
  foreach ($diccionario->valores as $key => $value) {
    $template = str_replace('{'.trim($key).'}', $value, $template);
  }
  print $template;

?>
